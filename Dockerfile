FROM openjdk:17-jdk-alpine

#ARG JAR_FILE=build/libs/yandexSchool-0.0.1-SNAPSHOT.jar
ARG JAR_FILE=yandexSchool-0.0.1-SNAPSHOT.jar
# cd /usr/local/runme
WORKDIR /usr/local/runme

# copy target/find-links.jar /usr/local/runme/app.jar
COPY ${JAR_FILE} app.jar


# java -jar /usr/local/runme/app.jar
ENTRYPOINT ["java","-jar","app.jar"]
